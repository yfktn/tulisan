<?php namespace Yfktn\Tulisan\Controllers;

use Backend\Classes\Controller;
use BackendMenu;
use Cms\Classes\Controller as CmsClassesController;
use Cms\Classes\Theme;
use Cms\Classes\Page as CmsPage;
use Yfktn\Tulisan\Models\Tulisan as TulisanModel;
use Flash;
use Log;
use October\Rain\Exception\AjaxException;
use October\Rain\Exception\ApplicationException;

class Tulisan extends Controller
{
    public $implement = [        'Backend\Behaviors\ListController',        'Backend\Behaviors\FormController'    ];
    
    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';

    public $requiredPermissions = [
        'yfktn.tulisan.tulisan_buat' 
    ];

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('Yfktn.Tulisan', 'main-menu-tulisan');
    }
    
    /**
     * Lakukan pembatasan pada daftar tampilan
     * @param type $query
     */
    public function listExtendQuery($query) {
        if(!$this->user->hasAnyAccess(['yfktn.tulisan.tulisan_akses_user_lain'])) {
//            trace_log("HOI!User ini tidak memiliki yfktn.tulisan.tulisan_akses_user_lain");
            $query->where('user_id', $this->user->id);
        }
    }
    
    /**
     * Sebelum melakukan pengambilan model yang diload pada form, pastikan bahwa
     * user yang mengakses memang hanya user yang berhak
     * @param type $query
     */
    public function formExtendQuery($query) {
        if(!$this->user->hasAnyAccess(['yfktn.tulisan.tulisan_akses_user_lain'])) {
            $query->where('user_id', $this->user->id);
        }
    }

    public function formExtendFields($form)
    {
        // ditaruh di sini karena kita tidak bisa melakukan setting terhadap customTags 
        // melalui filterFields.
        $tag =[
            'label' => 'Tag Tulisan',
            'nameFrom' => 'nama',
            'span' => 'auto',
            'mode' => 'relation',
            'type' => 'taglist',
            'separator' => 'comma',
            'customTags' => false,
            'comment' => 'Gunakan Tag untuk mengklasifikasikan dokumen',
            'tab' => 'Pengkategorian',
        ];
        
        // Tentukan dulu untuk masalah customTags
        // user tanpa hak akses ini tidak bisa melakukan penambahan terhadap customTag
        if ($this->user->hasAnyAccess(['yfktn.tulisan.kategori_manajemen', 'yfktn.tulisan.tags_baru'])) {
            $tag['customTags'] = true;
        }
        
        $form->addTabFields([
            'tag' => $tag
        ]);
    }
    
    public function index_onDelete() {
        if (($postDiCheck = post('checked')) && is_array($postDiCheck) && count($postDiCheck)) {
            $deleted = 0;
            foreach ($postDiCheck as $postId) {
                $post = TulisanModel::find($postId);
                if($post !== null && $post->canEdit($this->user)) {
                    // delete!
                    $post->delete();
                    $deleted++;
                }
            }
            if($deleted > 0) {
                Flash::success("Telah dilakukan penghapusan {$deleted} post!");
            }
        }
        return $this->listRefresh(); // refresh list kita!
    }
    
    public function listInjectRowClass($record, $definition = null) {
        if (!$record->ditampilkan) {
            return 'safe disabled';
        }
    }
    
    /**
     * Sebelum pertama kali dibuat
     * @param type $model
     */
    public function formBeforeCreate($model) {
        $model->user_id = $this->user->id;
    }

    /**
     * Lakukan preview terhadap tulisan, dengan melakukan redirect ke halaman yang aktif milik template yang 
     * terpilih saat ini!
     * 1. Load daftar page dari theme yg aktif
     * 2. check bila pada page nya ada component TulisanDetail
     * 3. lakukan loading ke URL nya! Jangan lupa untuk menambahkan "forPreview=true" untuk url nya
     * @param string $postId id post yang terpilih!
     * @return void 
     */
    public function onPreview()
    {

        $theme = Theme::getActiveTheme();

        $pages = CmsPage::listInTheme($theme, true);
        $cmsPage = null;

        foreach ($pages as $page) {
            if (!$page->hasComponent('tulisanDetail')) {
                continue;
            }

            /*
                 * Component must use a categoryPage filter with a routing parameter and post slug
                 * eg: slug = "{{ :somevalue }}"
                 */
            $properties = $page->getComponentProperties('tulisanDetail');
            if (!preg_match('/{{\s*:/', $properties['slug'])) {
                continue;
            }

            $cmsPage = $page;
            break;
        }

        if($cmsPage === null) {
            // tidak ada maka tampilkan view Error tidak ditemukan!
            throw new ApplicationException(
                'Tidak ada page pada CMS theme active untuk menampilkan Detail Tulisan ditemukan!'
            );
        }
        $found = TulisanModel::where('id', post('recordId', 0));
        // dapatkan hak akses saat ini!
        if (!$this->user->hasAnyAccess(['yfktn.tulisan.tulisan_akses_user_lain'])) {
            $found = $found->where('user_id', $this->user->id);
        }
        $found = $found->first();
        if($found === null) {
            // error
            throw new ApplicationException(
                'Tidak ada data ditemukan!'
            );
        }

        // ambil controller dari cms based untuk melakukan generate url halaman!
        $controller = CmsClassesController::getController() ?: new CmsClassesController();
        // bila ada maka, redirect!
        return [
            'url' => $controller->pageUrl($cmsPage->fileName, ['slug' => $found->slug]) . "/?forPreview=true"
        ];
    }
}
