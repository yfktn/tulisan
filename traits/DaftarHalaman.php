<?php namespace Yfktn\Tulisan\Traits;
/**
/**
 * Dapatkan daftar halaman yang dibuat di CMS page
 *
 * @author toni
 */
trait DaftarHalaman {
    
    /**
     * Dapatkan daftar halaman yang ada di CMS page, bisa digunakan untuk melakukan
     * load terhadap pemilihan halaman detail atau guna yang lain
     */
    public function getDaftarHalaman() {
        return \Cms\Classes\Page::sortBy('baseFileName')->lists('baseFileName', 'baseFileName');
    }
}
