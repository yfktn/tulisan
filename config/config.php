<?php
/**
 * setting untuk plugin tulisan
 * 
 */

 return [
    // apa default timezone di tampilan
    // akan digunakan waktu mengambil tulisan yang muncul 
    // sesuai dengan tanggal muncul
    'defaultShowTimezone' => 'Asia/Jakarta',
     // merupakan setting untuk integrasi dengan plugin offline.sitesearch
    'offlineSiteSearchResult' => [
        // set nilai ini dengan url untuk detail sebuah record tulisan
        'url'         => 'berita/baca',
        // ini dengan nilai param detailnya, pilihan yang ada adalah: 
        // id atau slug
        'paramDetail' => 'slug',
        // kategori ini nantinya ditampilkan di halaman hasil pencarian!
        'provider' => 'Berita',
        // bila ingin menggunakan fungsi lain untuk meregister event set 
        // ini menjadi negatif
        'activated' => true
    ]
 ];
