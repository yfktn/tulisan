<?php

namespace Yfktn\Tulisan\Components;

use Yfktn\Tulisan\Models\Tulisan as TulisanModel;

/**
 * Tampilkan daftar tulisan
 *
 * @author toni
 */
class TulisanList extends \Cms\Classes\ComponentBase
{
    use \Yfktn\Tulisan\Traits\DaftarHalaman;

    public function componentDetails()
    {
        return [
            'name'        => 'Daftar Tulisan',
            'description' => 'Tampilkan daftar tulisan'
        ];
    }

    public function defineProperties()
    {
        return [
            'paramHalaman' => [
                'title' => 'Parameter Halaman',
                'description' => 'Parameter menunjukkan halaman aktif yang di load',
                'type' => 'string',
                'default' => '{{ :page }}'
            ],
            'jumlahItemPerHalaman' => [
                'title' => 'Item Perhalaman',
                'description' => 'Jumlah item perhalaman ditampilkan',
                'type' => 'string',
                'default' => 10
            ],
            'filterKategori' => [
                'title' => 'Filter kategori',
                'description' => 'Tuliskan slug dan bila lebih dari satu gunakan |',
                'type' => 'string'
            ],
            'abaikanKategori' => [
                'title' => 'Abaikan kategori',
                'description' => 'Tuliskan slug dan bila lebih dari satu gunakan |',
                'type' => 'string'
            ],
            'halamanDetail' => [
                'title' => 'Halaman Detail',
                'description' => 'Alamat menuju detail tulisan',
                'type' => 'dropdown',
                'default' => 'tulisan/detail'
            ],
            'tampilPagination' => [
                'title' => 'Tampilkan pagination',
                'description' => 'Apakah menampilkan pagination?',
                'type' => 'checkbox',
                'default' => 'true'
            ]
        ];
    }

    public function getHalamanDetailOptions()
    {
        return $this->getDaftarHalaman();
    }

    protected function siapkanVariable()
    {
        $this->page['filterKategori'] = $this->property('filterKategori');
        $this->page['tampilkanPagination'] = $this->property('tampilkanPagination', true);
        $this->page['abaikanKategori'] = $this->property('abaikanKategori');
        $this->page['paramHalaman'] = $this->paramName('paramHalaman');
        $this->page['halamanAktif'] = $this->property('paramHalaman', 1);
        $this->page['jumlahItemPerHalaman'] = $this->property('jumlahItemPerHalaman');
        $this->page['halamanDetail'] = $this->property('halamanDetail');
    }

    public function loadTulisan()
    {
        $posts = TulisanModel::with(['gambar_header', 'kategori'])
            ->yangSudahDitampilkan()
            ->listDiFrontEnd([
                'page' => $this->page['halamanAktif'],
                'jumlahItemPerHalaman' => $this->page['jumlahItemPerHalaman'],
                'filter' => [
                    'kategori_slug_filter' => $this->page['filterKategori'],
                    'kategori_slug_except' => $this->page['abaikanKategori']
                ],
                'order' => [
                    'tgl_tampil' => 'DESC'
                ]
            ]);
        // kita dapatkan daftar maka proses untuk melakukan setting sesuai 
        // dengan url daripada page detailnya!
        // TODO: gunakan getProperty untuk mengdapatkan setting url id dan slug
        $posts->each(function ($post) {
            $post->setUrl($this->page['halamanDetail'], $this->controller, [
                'id' => 'id', 'slug' => 'slug'
            ]);
        });
        return $posts;
    }

    public function onRun()
    {
        $this->siapkanVariable();
        $this->page['posts'] = $this->loadTulisan();
    }
}
