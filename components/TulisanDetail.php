<?php

namespace Yfktn\Tulisan\Components;

use Backend\Facades\Backend;
use Backend\Facades\BackendAuth;

/**
 * Load tulisan detail
 *
 * @author toni
 */
class TulisanDetail extends \Cms\Classes\ComponentBase
{
    //put your code here
    public function componentDetails()
    {
        return [
            'name'        => 'Detail Tulisan',
            'description' => 'Tampilkan detial tulisan'
        ];
    }

    public function defineProperties()
    {
        return [
            'slug' => [
                'title'       => 'Slug',
                'description' => 'Parameter slug',
                'default'     => '{{ :slug }}',
                'type'        => 'string',
            ],
            'id' => [
                'title' => 'Parameter Id Terpilih',
                'description' => 'Parameter id prokum terpilih',
                'type' => 'string',
                'default' => '{{ :id }}'
            ],
        ];
    }

    protected function siapkanVariable()
    {
        $this->page["slug"] = $this->property('slug');
        $this->page["id"] = $this->property('id');
        // is this for preview?
        $this->page['forPreview'] = input('forPreview', false);
    }

    public function loadTulisan()
    {
        $p = \Yfktn\Tulisan\Models\Tulisan::with(['gambar_header', 'gambar_lainnya', 'kategori'])
            ->where('slug', $this->page["slug"]);
        if($this->page['forPreview'] && BackendAuth::check()) {
            // ini untuk preview, maka lakukan pembacaan terhadap hak akses
            if(!BackendAuth::getUser()->hasAnyAccess(['yfktn.tulisan.tulisan_akses_user_lain'])) {
                // bila user ingin preview punya dia sendiri
                $p = $p->where('user_id', BackendAuth::getUser()->id);
            }
        } else {
            // bila bukan preview maka tampilkan hanya yang sudah di publish!
            $p = $p->yangSudahDitampilkan();
        }
        return $p->first();
    }

    public function onRun()
    {
        $this->siapkanVariable();
        $this->page['post'] = $this->loadTulisan();
    }
}
