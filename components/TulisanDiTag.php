<?php 
namespace Yfktn\Tulisan\Components;

use Cms\Classes\ComponentBase;
use Yfktn\Tulisan\Traits\DaftarHalaman;
use Yfktn\Tulisan\Models\Tulisan as TulisanModel;

class TulisanDiTag extends ComponentBase
{
    use DaftarHalaman;

    public $posts = null;

    public function componentDetails() 
    {
        return [
            'name'        => 'Daftar Tulisan Di Tag',
            'description' => 'Tampilkan daftar tulisan di tag'
        ];
    }

    public function defineProperties()
    {
        return [
            'paramHalaman' => [
                'title' => 'Parameter Halaman',
                'description' => 'Parameter menunjukkan halaman aktif yang di load',
                'type' => 'string',
                'default' => '{{ :page }}'
            ],
            'jumlahItemPerHalaman' => [
                'title' => 'Item Perhalaman',
                'description' => 'Jumlah item perhalaman ditampilkan',
                'type' => 'string',
                'default' => 10
            ],
            'filterTag' => [
                'title' => 'Filter tag',
                'description' => 'Tuliskan slug tag dan bila lebih dari satu gunakan |',
                'type' => 'string'
            ],
            'abaikanTag' => [
                'title' => 'Abaikan tag',
                'description' => 'Tuliskan slug tag dan bila lebih dari satu gunakan |',
                'type' => 'string'
            ],
            'filterKategori' => [
                'title' => 'Filter kategori',
                'description' => 'Tuliskan slug dan bila lebih dari satu gunakan |',
                'type' => 'string'
            ],
            'abaikanKategori' => [
                'title' => 'Abaikan kategori',
                'description' => 'Tuliskan slug dan bila lebih dari satu gunakan |',
                'type' => 'string'
            ],
            'halamanDetail' => [
                'title' => 'Halaman Detail',
                'description' => 'Alamat menuju detail tulisan',
                'type' => 'dropdown',
                'default' => 'tulisan/detail'
            ],
            'tampilPagination' => [
                'title' => 'Tampilkan pagination',
                'description' => 'Apakah menampilkan pagination?',
                'type' => 'checkbox',
                'default' => 'true'
            ]
        ];
    }

    public function getHalamanDetailOptions()
    {
        return $this->getDaftarHalaman();
    }

    protected function siapkanVariable()
    {
        $this->page['tulisanDiTag'] = [
            'filterTag' => $this->property('filterTag'),
            'tampilkanPagination' => $this->property('tampilkanPagination', true),
            'abaikanTag' => $this->property('abaikanTag'),
            'paramHalaman' => $this->paramName('paramHalaman'),
            'halamanAktif' => $this->property('paramHalaman', 1),
            'jumlahItemPerHalaman' => $this->property('jumlahItemPerHalaman'),
            'halamanDetail' => $this->property('halamanDetail'),
            'filterKategori' => $this->property('filterKategori'),
            'abaikanKategori' => $this->property('abaikanKategori')
        ];
    }

    public function loadTulisan()
    {
        $posts = TulisanModel::with(['gambar_header', 'kategori'])
            ->yangSudahDitampilkan()
            ->listDiFrontEnd([
                'page' => $this->page['tulisanDiTag']['halamanAktif'],
                'jumlahItemPerHalaman' => $this->page['tulisanDiTag']['jumlahItemPerHalaman'],
                'filter' => [
                    'tag_slug_filter' => $this->page['tulisanDiTag']['filterTag'],
                    'tag_slug_except' => $this->page['tulisanDiTag']['abaikanTag'],
                    'kategori_slug_filter' => $this->page['tulisanDiTag']['filterKategori'],
                    'kategori_slug_except' => $this->page['tulisanDiTag']['abaikanKategori']
                ],
                'order' => [
                    'tgl_tampil' => 'DESC'
                ]
            ]);
        // kita dapatkan daftar maka proses untuk melakukan setting sesuai 
        // dengan url daripada page detailnya!
        // TODO: gunakan getProperty untuk mengdapatkan setting url id dan slug
        $posts->each(function ($post) {
            $post->setUrl($this->page['tulisanDiTag']['halamanDetail'], $this->controller, [
                'id' => 'id', 'slug' => 'slug'
            ]);
        });
        // dd($posts->count());
        return $posts;
    }

    public function onRun()
    {
        $this->siapkanVariable();
        $this->posts = $this->loadTulisan();
    }
    
}