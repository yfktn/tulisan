<?php
namespace Yfktn\Tulisan\Components;

use Cms\Classes\ComponentBase;
use Illuminate\Support\Facades\DB;
use Yfktn\Tulisan\Traits\DaftarHalaman;

class DaftarTag extends ComponentBase
{
    use DaftarHalaman;

    public $tags = null;

    public function componentDetails() 
    {
        return [
            'name'        => 'Daftar Tag',
            'description' => 'Tampilkan daftar tag'
        ];
    }

    public function defineProperties()
    {
        return [
            'slug' => [
                'title' => 'Slug',
                'description' => 'Parameter slug milik tag',
                'type' => 'string',
                'default' => '{{ :slug }}'
            ],
            'tampilkanYangKosong' => [
                'title' => 'Tampilkan Tag Kosong',
                'description' => 'Centang dan tag ditampilkan walaupun kosong',
                'type' => 'checkbox',
                'default' => 1,
            ],
            'halamanTag' => [
                'title' => 'Halaman Tag',
                'description' => 'Pilih halaman daftar tulisan atas tag terpilih',
                'type' => 'dropdown',
                'default' => 'tulisan/tag'
            ],
        ];
    }

    public function getHalamanTagOptions()
    {
        return $this->getDaftarHalaman();
    }

    public function siapkanVariable()
    {
        $this->page['tag'] = [
            'slug' => $this->property('slug'),
            'tampilkanYangKosong' => $this->property('tampilkanYangKosong'),
            'halamanTag' => $this->property('halamanTag')
        ];
    }

    public function loadPosts()
    {

        // lakukan loading tags dan dapatkan jumlah dokumen didalamnya ...
        $posts = DB::table('yfktn_tulisan_tag')
            ->join('yfktn_tulisan_tagnya', 'yfktn_tulisan_tag.id', '=', 'yfktn_tulisan_tagnya.tag_id')
            ->join('yfktn_tulisan_tulis', 'yfktn_tulisan_tulis.id', '=', 'yfktn_tulisan_tagnya.tulisan_id')
            ->select(Db::raw('yfktn_tulisan_tag.nama, yfktn_tulisan_tag.slug, yfktn_tulisan_tag.id, count(*) as jumlah, null as url'))
            ->groupBy('yfktn_tulisan_tag.nama', 'yfktn_tulisan_tag.slug', 'yfktn_tulisan_tag.id')
            ->get();
        $posts->each(function ($item) {
            $item->url = $this->controller->pageUrl($this->page['tag']['halamanTag'], [
                'tagid' => $item->id,
                'tagslug' => $item->slug,
                'slug' => $item->slug
            ]);
        });
        return $posts;
    }

    public function onRun()
    {
        $this->siapkanVariable();
        $this->tags = $this->loadPosts();
    }

    
}