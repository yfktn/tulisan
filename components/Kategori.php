<?php namespace Yfktn\Tulisan\Components;
use Yfktn\Tulisan\Models\Kategori as KategoriModel;
/**
 * Menampilkan kategori untuk tulisan, masih belum memberikan fasilitas render
 * berdasarkan struktur tree daripada kategori!
 *
 * @author toni
 */
class Kategori extends \Cms\Classes\ComponentBase {
    use \Yfktn\Tulisan\Traits\DaftarHalaman;
    
    public function componentDetails() {
        return [
            'name'        => 'Tampilkan Kategori',
            'description' => 'Tampilkan daftar kategori'
        ];
    }
    
    public function defineProperties() {
        return [
            'slug' => [
                'title' => 'Slug',
                'description' => 'Parameter slug milik kategori',
                'type' => 'string',
                'default' => '{{ :slug }}'
            ],
            'tampilkanYangKosong' => [
                'title' => 'Tampilkan Kategori Kosong',
                'description' => 'Centang dan kategori ditampilkan walaupun kosong',
                'type' => 'checkbox',
                'default' => 1,
            ],
            'filterKategori' => [
                'title' => 'Filter kategori',
                'description' => 'Tuliskan slug dan bila lebih dari satu gunakan |',
                'type' => 'string'
            ],
            'abaikanKategori' => [
                'title' => 'Abaikan kategori',
                'description' => 'Tuliskan slug dan bila lebih dari satu gunakan |',
                'type' => 'string'
            ],
            'halamanKategori' => [
                'title' => 'Halaman Kategori',
                'description' => 'Alamat menuju daftar tulisan pada kategori',
                'type' => 'dropdown',
                'default' => 'tulisan/kategori'
            ]
        ];
    }
    
    public function getHalamanKategoriOptions() {
        return $this->getDaftarHalaman();
    }
    
    protected function siapkanVariable() {
        $this->page['slug'] = $this->property('slug');
        $this->page['filterKategori'] = $this->property('filterKategori');
        $this->page['abaikanKategori'] = $this->property('abaikanKategori');
        $this->page['halamanKategori'] = $this->property('halamanKategori');
        $this->page['tampilkanYangKosong'] = $this->property('tampilkanYangKosong');
    }
    
    protected function loadKategori() {
        $katres = KategoriModel::with('jumlah_tulisan')
                ->listDiFrontEnd([
                    'tampilkanYangKosong' => (bool) $this->page['tampilkanYangKosong'],
                    'filter' => [
                        'filterKategori' => $this->page['filterKategori'],
                        'abaikanKategori' => $this->page['abaikanKategori'],
                    ]
                ])
                ->get();
        $currUrl = request()->url();
        // set url nya
        $katres->each(function($k) use($currUrl) {
            $durl = $k->setUrl($this->page['halamanKategori'], $this->controller, [
                'id' => 'id', 'slug' => 'slug'
            ]);
            $k->ini_lagi_halaman_kategorinya = strcasecmp($currUrl, $durl) === 0;
        });
        return $katres;
    }


    public function onRun() {
        $this->siapkanVariable();
        $this->page['kategori'] = $this->loadKategori();
    }
}
