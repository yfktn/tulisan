Ini merupakan plugin untuk manajemen tulisan, di mana salah satu tujuannya untuk memberikan pengelolaan tulisan di Backend, dikembangkan sebagai sebuah OctoberCMS plugin. 

Memiliki fasilitas untuk menyimpan judul, slug, isi tulisan dengan WYSIWYG, pengaturan kategori tulisan, header image dan gambar berkaitan. Namun tentu saja, agar bisa digunakan dengan baik maka pengguna diharuskan untuk melakukan perubahan pada TWIG template yang digunakan untuk melakukan render menggunakan component yang ada.

Terdapat 3 component sebagaimana layaknya sebuah manajemen tulisan:

- TulisanList : digunakan untuk menampilkan daftar tulisan
- TulisanDetail: digunakan untuk menampilkan isi tulisan
- Kategori: digunakan untuk menampilkan daftar kategori

### Melakukan Override Template Component

Seperti yang disampikan bahwa component tidak otomatis memberikan sebuah hasil render yang sesuai dengan template yang digunakan oleh pengguna. 

Misalnya pada sebuah `page` di sebuah homepage menggunakan component `TulisanList` dan dideklarasikan pada `page`:

```
[TulisanList daftarTulisanHomepage]
paramHalaman = "{{ :page }}"
jumlahItemPerHalaman = 10
filterKategori = "{{ :slugkategori }}"
halamanDetail = "kabar-baca"
tampilPagination = "true"
```
Maka untuk melakukan override template bisa dilakukan dengan langkah:

- buat sebuah folder pada `themes/digunakan/partials/daftarTulisanHomepage`
- copy file `default.htm` dari `plugins/yfktn/tulisan/components/tulisanlist/default.htm` ke foder yang baru dibuat tersebut
- melakukan penyesuaian pada isi `default.htm` tersebut

### Preview sesuai theme terpilih

Untuk melakuan preview tulisan maka tulisan harus tersimpan dan pada bagian page di CMS, sudah ada yang mengimplementasikan component TulisanDetail.

### integrasi dengan Offline.SiteSearch

Plugin Offline.SiteSearch memberikan kita fasilitas untuk membuat sebuah fasilitas pencarian dengan highlight, silahkan check pada plugin tersebut.

Plugin `tulisan` ini secara default telah terintegrasi dengan plugin tersebut, yang perlu dilakukan tinggal melakukan setting link URL, agar hasil pencarian saat di klik dapat mengakses link yang digunakan pada aplikasi saat itu.

Silahkan copy `plugins/yfktn/tulisan/config/config.php` ke path konfigurasi aplikasi di `config/yfktn/tulisan/config.php` sehingga saat plugin di update, perubahan kita tidak tertimpa. Silahkan ubah sesuaikan konfigurasi.