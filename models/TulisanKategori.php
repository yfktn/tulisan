<?php namespace Yfktn\Tulisan\Models;

use Model;

/**
 * Model
 */
class TulisanKategori extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;


    /**
     * @var string The database table used by the model.
     */
    public $table = 'yfktn_tulisan_tulis_kategori';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];
}
