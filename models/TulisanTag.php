<?php namespace Yfktn\Tulisan\Models;

use Model;
use October\Rain\Database\Traits\Sluggable;

/**
 * TulisanTag Model
 */
class TulisanTag extends Model
{
    use \October\Rain\Database\Traits\Validation;
    use Sluggable;

    protected $slugs = [
        'slug' => 'nama'
    ];

    /**
     * @var string table associated with the model
     */
    public $table = 'yfktn_tulisan_tag';

    public $timestamps = false;

    /**
     * @var array guarded attributes aren't mass assignable
     */
    protected $guarded = ['*'];

    /**
     * @var array fillable attributes are mass assignable
     */
    protected $fillable = ['nama'];

    /**
     * @var array rules for validation
     */
    public $rules = [
        'nama' => 'required|min:4'
    ];

    /**
     * @var array Attributes to be cast to native types
     */
    protected $casts = [];

    /**
     * @var array jsonable attribute names that are json encoded and decoded from the database
     */
    protected $jsonable = [];

    /**
     * @var array appends attributes to the API representation of the model (ex. toArray())
     */
    protected $appends = [];

    /**
     * @var array hidden attributes removed from the API representation of the model (ex. toArray())
     */
    protected $hidden = [];

    /**
     * Dapatkan nama tag menggunakan id
     * @param mixed $val 
     * @return mixed 
     */
    public static function getSelectedTagNameByTagId($id)
    {
        return optional(TulisanTag::find($id))->nama;
    }

}
