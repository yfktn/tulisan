<?php namespace Yfktn\Tulisan\Models;

use Model;
use BackendAuth;
use Backend\Models\User;
use Log;
use October\Rain\Database\Traits\Sluggable;
use ValidationException;
/**
 * Model
 */
class Tulisan extends Model
{
    use \October\Rain\Database\Traits\Validation;
    use Sluggable;

    protected $slugs = [
        'slug' => 'judul'
    ];
    /**
     * @var string The database table used by the model.
     */
    public $table = 'yfktn_tulisan_tulis';

    /**
     * @var array Validation rules
     */
    public $rules = [
        'judul'   => 'required',
        'slug'    => ['required', 'regex:/^[a-z0-9\/\:_\-\*\[\]\+\?\|]*$/i', 'unique:yfktn_tulisan_tulis'],
        'isi' => 'required',
        'excerpt' => ''
    ];
    
    public $belongsTo = [
        'user' => ['Backend\Models\User', 'key' => 'user_id']
    ];
    
    public $attachOne = [
        'gambar_header' => ['System\Models\File']
    ];
    
    public $attachMany = [
        'gambar_lainnya' => ['System\Models\File']
    ];
    
    public $belongsToMany = [
        'kategori' => [
            'Yfktn\Tulisan\Models\Kategori',
            'key' => 'tulisan_id',
            'otherKey' => 'kategori_id',
            'table' => 'yfktn_tulisan_tulis_kategori',
            'order' => 'nama'
        ],
        'tag' => [
            'Yfktn\Tulisan\Models\TulisanTag',
            'key' => 'tulisan_id',
            'otherKey' => 'tag_id',
            'table' => 'yfktn_tulisan_tagnya',
            'order' => 'nama'
        ]
    ];
    
    /**
     * Tampilkan formulir sesuai dengan hak akses pengguna saat ini!
     * @param type $fields
     * @param type $context
     * @return type
     */
    public function filterFields($fields, $context = null) {

        $user = BackendAuth::getUser();

        // baru tentukan untuk management nya!
        if(!isset($fields->ditampilkan, $fields->tgl_tampil)) {
            return;
        }
        if($user->hasAnyAccess(['yfktn.tulisan.tulisan_manajemen'])) {
            $fields->ditampilkan->hidden = false;
            $fields->tgl_tampil->hidden = false;
        }
        else {
            $fields->ditampilkan->hidden = true;
            $fields->tgl_tampil->hidden = true;
        }

    }

    public function afterValidate()
    {
        if (BackendAuth::getUser()->hasAnyAccess(['yfktn.tulisan.tulisan_manajemen'])) {
            if (
                $this->ditampilkan &&
                !$this->tgl_tampil
            ) {
                throw new ValidationException([
                    'tgl_tampil' => 'Ditampilkan tapi tidak dipilih tanggalnya!'
                ]);
            }
        } else {
            if ($this->isDirty('ditampilkan') || $this->isDirty('tgl_tampil')) {
                // kalau bukan seorang manajer, tidak boleh melakukan perubahan pada 
                // status terpublish dan tanggal tampilnya!
                $this->ditampilkan = $this->getOriginal('ditampilkan');
                $this->tgl_tampil = $this->getOriginal('tgl_tampil');
                // Bug ditampilkan tidak boleh null!
                if( $this->ditampilkan == null ) {
                    $this->ditampilkan = false;
                }
                // jangan menampilkan validation exception karena ini terkait
                // dengan editing punya non manajer
                // throw new ValidationException([
                //     'tgl_tampil' => 'Bukan manajer tapi mengubah setting manajer?!'
                // ]);
            }
        }
    }
    
    /**
     * Siapa yang bisa melakukan editing terhadap suatu post?
     * @param User $user
     * @return bool
     */
    public function canEdit(User $user) {
        return ($this->user_id == $user->id) ||
            $user->hasAnyAccess(['yfktn.tulisan.tulisan_manajemen']);
    }
    
    /**
     * Dapatkan record yang sudah saatnya ditampilkan!
     * @param type $query
     */
    public function scopeYangSudahDitampilkan($query) {
        // convert nilai tanggal yang diinginkan supaya sama dengan 
        // nilai di database terkait timezone nya
        $userTimezone = config("yfktn.tulisan::defaultShowTimezone", "Asia/Jakarta");
        $dbTimezone = config("app.timezone");
        $waktuSaatIni = \Carbon\Carbon::now($userTimezone);
        // convert ke dbtimezone
        $waktuSaatIniUntukDicheckKeDb = $waktuSaatIni->timezone($dbTimezone);
        // masukkan ke query
        $query->whereNotNull('ditampilkan')
                ->where('ditampilkan', 1)
                ->whereNotNull('tgl_tampil')
                ->where('tgl_tampil', '<=', $waktuSaatIniUntukDicheckKeDb->toDateString());
                //->where('tgl_tampil', '<=', date('Y-m-d'));
    }
    
    /**
     * Untuk tampilan di halaman depan front end
     * @param mixed $query 
     * @param array $opsi 
     * @return mixed 
     */
    public function scopeListDiFrontEnd($query, $opsi=[]) {
        extract(array_merge([
            'page' => 1,
            'jumlahItemPerHalaman' => 10,
            'filter' => [],
            'order' => []
        ], $opsi));
        
        if(sizeof($filter) > 0) {
            // lakukan filter terhadap kategori berdasarkan slug
            if(array_key_exists('kategori_slug_filter', $filter) 
                    && strlen($filter['kategori_slug_filter'])>0) {
                $slugs = explode("|", $filter['kategori_slug_filter']);
                if(sizeof($slugs) > 0) {
                    $slugs = array_filter($slugs, function($item) {
                        return strlen($item) >= 1;
                    });
                    $query->whereHas('kategori', function($query) use($slugs) {
                        // ingat bahwa filter lebih dari satu slug kategori menggunakan
                        // tanda |, sehingga pisahkan ini dulu
                        $query->whereIn('slug', $slugs);
                    });
                }
            }
            // Jangan tampilkan record yang tidak termasuk dalam kategori yang
            // dikecualikan oleh user
            if(array_key_exists('kategori_slug_except', $filter) && strlen($filter['kategori_slug_except'])>0) {
                $slugs = explode("|", $filter['kategori_slug_except']);
                if(sizeof($slugs) > 0) {
                    $slugs = array_filter($slugs, function($item) {
                        return strlen($item) >= 1;
                    });
                    $query->whereDoesntHave('kategori', function($query) use($slugs) {
                        // ingat bahwa filter lebih dari satu slug kategori menggunakan
                        // tanda |, sehingga pisahkan ini dulu
                        $query->whereIn('slug', $slugs);
                    });
                }
            }
            // untuk tag nya juga bisa di sini
            if (
                array_key_exists('tag_slug_filter', $filter)
                && strlen($filter['tag_slug_filter']) > 0
            ) {
                $slugs = explode("|", $filter['tag_slug_filter']);
                if (sizeof($slugs) > 0) {
                    $slugs = array_filter($slugs, function ($item) {
                        return strlen($item) >= 1;
                    });
                    $query->whereHas('tag', function ($query) use ($slugs) {
                        // ingat bahwa filter lebih dari satu slug kategori menggunakan
                        // tanda |, sehingga pisahkan ini dulu
                        $query->whereIn('slug', $slugs);
                    });
                }
            }
            // Jangan tampilkan record yang tidak termasuk dalam kategori yang
            // dikecualikan oleh user
            if (array_key_exists('tag_slug_except', $filter) && strlen($filter['tag_slug_except']) > 0) {
                $slugs = explode("|", $filter['tag_slug_except']);
                if (sizeof($slugs) > 0) {
                    $slugs = array_filter($slugs, function ($item) {
                        return strlen($item) >= 1;
                    });
                    $query->whereDoesntHave('tag', function ($query) use ($slugs) {
                        // ingat bahwa filter lebih dari satu slug kategori menggunakan
                        // tanda |, sehingga pisahkan ini dulu
                        $query->whereIn('slug', $slugs);
                    });
                }
            }
            
        }
        
        if($order != []) {
            foreach ($order as $k=>$v ) {
                $query->orderBy($k, $v);
            }
        }
        
        // kembalikan hasil paginate
        return $query->paginate($jumlahItemPerHalaman, $page);
    }
    
    public function getUrlAttribute() {
        return isset($this->attributes['url']) ? $this->attributes['url']: '';
    }
    
    public function setUrl($pageName, $controller, $urlParams = []) {
        $params = [
            array_get($urlParams, 'id', 'id') => $this->id,
            array_get($urlParams, 'slug', 'slug') => $this->slug,
        ];
        $params['kategori'] = ($this->kategori->count() ? $this->kategori->first()->slug: null);
        
        return $this->attributes['url'] = $controller->pageUrl($pageName, $params);
    }
    
    public function scopeFilterKategori($query, $kategoriId) {
//        return $query->whereHas('kategori', function($q) use ($kategoriId) {
//            $q->whereIn('id', $kategoriId);
//        });
        return $query->join('yfktn_tulisan_tulis_kategori',
                'yfktn_tulisan_tulis_kategori.tulisan_id', '=', 'yfktn_tulisan_tulis.id')
                ->whereIn('yfktn_tulisan_tulis_kategori.kategori_id', $kategoriId );
    }

    /**
     * Lakukan filter berdasarkan tag id
     * @param mixed $query 
     * @param mixed $tagId 
     * @return mixed 
     */
    public function scopeFilterTag($query, $tagId) {
        return $query->join('yfktn_tulisan_tagnya',
                'yfktn_tulisan_tagnya.tulisan_id', '=', 'yfktn_tulisan_tulis.id')
                ->whereIn('yfktn_tulisan_tagnya.tag_id', $tagId );
    }
    /**
     * Kembalikan nilai ringkasan yang dipaksa harus ada, karena Bila tidak ada ringkasan diisi
     * maka kembalikan paragraph pertama dari isi kita, hilangkan semua tag HTML nya!
     *
     * @return string
     */
    public function getRingkasanPaksaAttribute() {
        if(strlen($this->attributes['ringkasan'])>0) {
            return $this->attributes['ringkasan'];
        }
        $end = strpos($this->attributes['isi'], '</p>', 4);
        return strip_tags(substr($this->attributes['isi'], 0, $end));
    }

    /**
     * Dapatkan item sebelum atau sesudah post yang aktif saat itu!
     * Dapatkan item setelah nya ...
     * Post::dapatkanTetangga()->first()
     * 
     * Dapatkan item sebelumnya
     * Post::dapatkanTetangga(-1)->first()
     * 
     * Explicit dengan attribute id, default tgl_tampil
     * Post::dapatkanTetangga(['arah'=>-1, 'attribute'=>'id'])
     * @param mixed $query 
     * @param array $options 
     * @return void 
     */
    public function scopeDapatkanTetangga($query, $options = [])
    {
        if(!is_array($options)) {
            $options = ['arah' => $options];
        }

        extract(array_merge([
            'arah' => 'berikutnya',
            'attribute' => 'tgl_tampil'
        ], $options));

        $isPrevious = in_array($arah, ['sebelumnya', -1]);
        $directionOrder = $isPrevious ? 'asc' : 'desc';
        $directionOperator = $isPrevious ? '>' : '<';

        // jangan ambil post yang saat ini aktif
        $query->where('id', '<>', $this->id);

        // check bahwa nilai yang menjadi acuan itu tidak null!
        if(!is_null($this->$attribute)) {
            $query->where($attribute, $directionOperator, $this->$attribute);
        }

        return $query->orderBy($attribute, $directionOrder);
    }

    /**
     * Dapatkan post berikutnya yang sudah dipublish, ini langsung mengembalikan post
     * @return mixed 
     */
    public function postBerikutnya()
    {
        return self::yangSudahDitampilkan()->dapatkanTetangga()->first();
    }

    /**
     * Dapatkan post sebelumnya yang sudah dipublish, ini langsung mengembalikan post
     * @return mixed 
     */
    public function postSebelumnya()
    {
        return self::yangSudahDitampilkan()->dapatkanTetangga(-1)->first();
    }
}
