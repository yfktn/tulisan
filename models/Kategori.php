<?php namespace Yfktn\Tulisan\Models;

use Model;
use October\Rain\Database\Traits\Sluggable;

/**
 * Model
 */
class Kategori extends Model
{
    use \October\Rain\Database\Traits\Validation;
    use \October\Rain\Database\Traits\NestedTree;
    use Sluggable;
    
    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;

    protected $slugs = [
        'slug' => 'nama'
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'yfktn_tulisan_kategori';

    /**
     * @var array Validation rules
     */
    public $rules = [
        'nama' => 'required',
        'slug' => 'required|unique:yfktn_tulisan_kategori'
    ];
    
    public $belongsToMany = [
        'tulisan' => ['Yfktn\Tulisan\Models\Tulisan',
            'key' => 'kategori_id',
            'otherKey' => 'tulisan_id',
            'table' => 'yfktn_tulisan_tulis_kategori',
            'order' => 'tgl_tampil desc',
            'scope' => 'yangSudahDitampilkan'
        ],
        'jumlah_tulisan' => ['Yfktn\Tulisan\Models\Tulisan',
            'key' => 'kategori_id',
            'otherKey' => 'tulisan_id',
            'table' => 'yfktn_tulisan_tulis_kategori',
            'scope' => 'yangSudahDitampilkan',
            'count' => true
        ],
    ];
    
    public function getJumlahTulisanAttribute() {
        return optional($this->jumlah_tulisan()->first())->count ?? 0;
    }

    public function setUrl($pageName, $controller, $urlParams = []) {
        $params = [
            array_get($urlParams, 'id', 'id') => $this->id,
            array_get($urlParams, 'slug', 'slug') => $this->slug,
        ];
        return $this->attributes['url'] = $controller->pageUrl($pageName, $params);
    }
    
    public function getUrlAttribute() {
        return isset($this->attributes['url']) ? $this->attributes['url']: '';
    }
    
    public function setIniLagiHalamanKategorinyaAttribute($val) {
        $this->attributes['ini_lagi_halaman_kategorinya'] = $val;
    }
    
    public function getIniLagiHalamanKategorinyaAttribute() {
        return isset($this->attributes['ini_lagi_halaman_kategorinya']) ?
            $this->attributes['ini_lagi_halaman_kategorinya'] : false;
    }


    public function scopeListDiFrontEnd($query, $opsi=[]) {
        extract(array_merge([
            'tampilkanYangKosong' => false,
            'filter' => []
        ], $opsi));
        if($tampilkanYangKosong===false) {
            $query->has('tulisan');
        }
        if(sizeof($filter) > 0) {
            // lakukan filter terhadap kategori berdasarkan slug
            if(array_key_exists('filterKategori', $filter) 
                    && strlen($filter['filterKategori'])>0) {
                $slugs = explode("|", $filter['filterKategori']);
                if(sizeof($slugs) > 0) {
                    $slugs = array_filter($slugs, function($item) {
                        return strlen($item) >= 1;
                    });
                    $query->whereIn('slug', $slugs);
                }
            }
            // Jangan tampilkan record yang tidak termasuk dalam kategori yang
            // dikecualikan oleh user
            if(array_key_exists('abaikanKategori', $filter) && strlen($filter['abaikanKategori'])>0) {
                $slugs = explode("|", $filter['abaikanKategori']);
                if(sizeof($slugs) > 0) {
                    $slugs = array_filter($slugs, function($item) {
                        return strlen($item) >= 1;
                    });
                    $query->whereNotIn('slug', $slugs);
                }
            }
        }
        $query->orderBy('nest_left', 'asc');
    }
}
