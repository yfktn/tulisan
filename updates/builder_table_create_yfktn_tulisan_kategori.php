<?php namespace Yfktn\Tulisan\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateYfktnTulisanKategori extends Migration
{
    public function up()
    {
        Schema::create('yfktn_tulisan_kategori', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string('nama', 100)->index();
            $table->string('slug', 120)->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('yfktn_tulisan_kategori');
    }
}