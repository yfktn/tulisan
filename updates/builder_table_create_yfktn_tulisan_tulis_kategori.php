<?php namespace Yfktn\Tulisan\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateYfktnTulisanTulisKategori extends Migration
{
    public function up()
    {
        Schema::create('yfktn_tulisan_tulis_kategori', function($table)
        {
            $table->engine = 'InnoDB';
            $table->integer('kategori_id')->index()->unsigned();
            $table->integer('tulisan_id')->index()->unsigned();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('yfktn_tulisan_tulis_kategori');
    }
}