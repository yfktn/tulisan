<?php namespace Yfktn\Tulisan\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateYfktnTulisanTulis extends Migration
{
    public function up()
    {
        Schema::create('yfktn_tulisan_tulis', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string('judul');
            $table->string('slug')->index();
            $table->text('isi');
            $table->text('ringkasan')->nullable();
            $table->smallInteger('ditampilkan')->index()->unsigned()->default(0);
            $table->date('tgl_tampil')->nullable();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->integer('user_id')->unsigned();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('yfktn_tulisan_tulis');
    }
}