<?php namespace Yfktn\Tulisan\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuatYfktnTulisanKategori extends Migration
{
    public function up()
    {
        Schema::table('yfktn_tulisan_kategori', function($table)
        {
            $table->integer('parent_id')->unsigned()->index()->nullable();
            $table->integer('nest_left')->unsigned()->nullable();
            $table->integer('nest_right')->unsigned()->nullable();
            $table->integer('nest_depth')->unsigned()->nullable();
        });
    }

    public function down()
    {
        Schema::table('yfktn_tulisan_kategori', function($table)
        {
            $table->dropColumn('parent_id');
            $table->dropColumn('nest_left');
            $table->dropColumn('nest_right');
            $table->dropColumn('nest_depth');
        });
    }
}