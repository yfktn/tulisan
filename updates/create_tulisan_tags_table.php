<?php namespace Yfktn\Tulisan\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

/**
 * CreateTulisanTagsTable Migration
 */
class CreateTulisanTagsTable extends Migration
{
    public function up()
    {
        Schema::create('yfktn_tulisan_tulisan_tags', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('yfktn_tulisan_tulisan_tags');
    }
}
