<?php

namespace Yfktn\Tulisan\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class YfktnIndexTulisanTglTampil extends Migration
{
    public function up()
    {
        Schema::table('yfktn_tulisan_tulis', function ($table) {
            $table->index(['tgl_tampil']);
        });
    }

    public function down()
    {
        Schema::table('yfktn_tulisan_tulis', function ($table) {
            $table->dropIndex('yfktn_tulisan_tulis_tgl_tampil_index');
        });
    }
}
