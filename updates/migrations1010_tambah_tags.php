<?php namespace Yfktn\Tulisan\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class Migrations1010TambahTags extends Migration
{
    public function up()
    {
        Schema::create('yfktn_tulisan_tag', function($table) 
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string('nama');
            $table->string('slug')->nullable()->index();
        });

        Schema::create('yfktn_tulisan_tagnya', function ($table) 
        {
            $table->engine = 'InnoDB';
            $table->integer('tulisan_id')->unsigned()->index();
            $table->integer('tag_id')->unsigned()->index();
        });

    }

    public function down()
    {
        Schema::dropIfExists('yfktn_tulisan_tagnya');
        Schema::dropIfExists('yfktn_tulisan_tag');
    }
}