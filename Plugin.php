<?php namespace Yfktn\Tulisan;

use System\Classes\PluginBase;
use \Yfktn\Tulisan\Models\Tulisan as TulisanModel;

class Plugin extends PluginBase
{
    public function registerComponents()
    {
        return [
            '\Yfktn\Tulisan\Components\TulisanList' => 'tulisanList',
            '\Yfktn\Tulisan\Components\TulisanDetail' => 'tulisanDetail',
            '\Yfktn\Tulisan\Components\Kategori' => 'kategori',
            '\Yfktn\Tulisan\Components\DaftarTag' => 'daftarTag',
            '\Yfktn\Tulisan\Components\TulisanDiTag' => 'tulisanDitag'
        ];
    }

    public function registerSettings()
    {
    }

    public function registerMarkupTags()
    {
        return [
            'filters' => [
                'namakategori' => function($slug) {
                    $res = \Yfktn\Tulisan\Models\Kategori::where('slug', '=', $slug)->first();
                    return $res == null? '':$res->nama;
                }
            ]
        ];
    }

    public function boot()
    {
        $tulisanOfflineSearchResult = \Config::get('yfktn.tulisan::offlineSiteSearchResult.activated', true);
        if($tulisanOfflineSearchResult) {
            // lakukan penambahan untuk mendengarkan pencarian dokumen
            \Event::listen('offline.sitesearch.query', function ($query) {
                $provider = \Config::get('yfktn.tulisan::offlineSiteSearchResult.provider', 'Tulisan');
                // lakukan query ke model milik tulisan
                $items = TulisanModel::yangSudahDitampilkan()->where(function($theq) use($query) {
                    $theq->where('judul', 'like', "%{$query}%")
                    ->orWhere('isi', 'like', "%{$query}%")
                    ->orWhere('ringkasan', 'like', "%{$query}%");
                })->get();
                // bangun hasilnya
                $results = $items->map(function ($item) use ($query) {
                    // If the query is found in the title, set a relevance of 2
                    $relevance = mb_stripos($item->judul, $query) !== false ? 2 : 1;
                    $generatedUrl = \Config::get('yfktn.tulisan::offlineSiteSearchResult.url');
                    if(\Config::get('yfktn.tulisan::offlineSiteSearchResult.paramDetail') == 'slug') {
                        $generatedUrl .= '/' . $item->slug;
                    } else {
                        $generatedUrl .= '/' . $item->id;
                    }
                    return [
                        'title' => $item->judul,
                        'text' => $item->isi,
                        'url' => $generatedUrl,
                        // 'thumb'     => $item->images->first(), // Instance of System\Models\File
                        'relevance' => $relevance, // higher relevance results in a higher
                                                   // position in the results listing
                        // 'meta' => 'data',       // optional, any other information you want
                                                   // to associate with this result
                        // 'model' => $item,       // optional, pass along the original model
                    ];
                });
                return [
                    'provider' => $provider,
                    'results' => $results
                ];
            });
        }

    }
}
